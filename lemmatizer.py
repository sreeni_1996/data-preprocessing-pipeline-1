import spacy
nlp = spacy.load('en_core_web_sm')


# Lemmatizer function which takes a content as arguement and returns the modified string
# if a word is a pronoun or mention it does not modify it ,for the hastages content alone it removes the hastag

class Lemmatizer():
    def __init__(self):
        pass
    def process(self,text):

        text1 = text
        #print("Lemmatization started")
        lema_string=""  #modified string which will be returned
        doc=nlp(text1)  #content is parsed using spacy model and stored in doc variable
        for token in doc:
            if(token.lemma_ == '-PRON-' or token.text[0]== '@'):
                lema_string+=token.text
                lema_string+=" "
            else:
                lema_string+=token.lemma_
                lema_string+=" "
        #print("Lemmatization finished")
        return lema_string