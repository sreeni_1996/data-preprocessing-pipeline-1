import re
import itertools
from nltk.corpus import stopwords


class Basic_Cleaning():
    def __init__(self):
        pass

    def process(self, text):

        # takes the input for cleaning
        stop = stopwords.words('english')
        #print("Basic Cleaning started")

        text1 = ""
        try:
            text1 = str(text)  # converted into string format
            text1 = re.sub(r'\b[\w\-.]+?@\w+?\.\w{2,4}\b', 'emailaddr', text1)  # email addresses handled
            text1 = re.sub(r'(http[s]?\S+)|(\w+\.[A-Za-z]{2,4}\S*)', 'httpaddr', text1)  # website links handeled
            text1 = re.sub(r'[!@#$%^&*,.()_\-<>?:;~`\[\]\(\)\\\/\"]', "", text1)  # special characters removed
            text1 = re.sub(r'([A-Z])', r' \1', text1)  # Joint words splitted
            text1 = text1.lower()  # text case handeled
            text1 = re.sub(r"(http)\w*", " ", text1)  # http tags removed
            text1 = re.sub(r"\bamp\b", " ", text1)  # remaining tags handled
            text1 = re.sub(r"\s+", " ", text1)  # whitespaces handled
            text1 = re.sub("\d+", "", text1)  # Removing digits
            text1 = ''.join(''.join(s)[:2] for _, s in itertools.groupby(text1))  # elongated/exagerated words handled
            text1 = ' '.join([word for word in text1.split() if word not in (stop)])  # stopwords removed
            text1  # Processed text appended to empty list
        except(TypeError):  # exception command incase of type error
            text1 = text
        #print("Basic Cleaning finished")

        return text1