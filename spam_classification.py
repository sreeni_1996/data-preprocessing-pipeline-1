import json
import numpy as np
import pandas as pd
from keras.models import model_from_json
from keras.preprocessing import sequence


class SpamHamModel():
    def __init__(self):
        pass
    def predictSpam(self,text):
        # load model parameters
        json_file = open(
            '/home/user/LV/Codes from Revlon/revlon_packages/spam_twitter/spamfilter_twitter_parameters.json', 'r')
        loaded_model_parameters = json_file.read()
        json_file.close()
        j = json.loads(loaded_model_parameters)

        # read from saved dictionary
        with open(j['path']['dictionary'], 'r') as dictionary_file:
            vocabulary = json.load(dictionary_file)

        # read saved model structure
        json_file = open(j['path']['model'], 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        # and create a model from that
        model = model_from_json(loaded_model_json)
        # weight the nodes with saved values
        model.load_weights(j['path']['weights'])

        # for human-friendly printing
        labels = [j['label']['1'], j['label']['2']]
        maxlen = j['max_len']

        data={'Contents':[text]}
        temp_df=pd.DataFrame.from_dict(data)
        input= temp_df['Contents'].tolist()

        output = np.array([[vocabulary.get(word,0) for word in sentence] for sentence in input])
        output = sequence.pad_sequences(output, maxlen=maxlen)

        # predict which bucket your input belongs in
        pred = model.predict(output)
        return labels[np.argmax(pred)]

