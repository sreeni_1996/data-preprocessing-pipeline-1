from html_parser import HTMLParser
from basic_cleaning import  Basic_Cleaning
from lemmatizer import Lemmatizer
from spam_classification import SpamHamModel
import pandas as pd


class DataPreprocess():

    def __init__(self):
        pass

    def main(self,text,sr):
        ob_html_parser = HTMLParser()
        ob_basic_clean=Basic_Cleaning()
        ob_lemmatizer=Lemmatizer()
        ob_spam = SpamHamModel()
        response_htmlparser = ob_html_parser.process(text)
        response_basic =ob_basic_clean.process(response_htmlparser)
        response_lemma=ob_lemmatizer.process(response_basic)
        spam_ham = ob_spam.predictSpam(response_lemma)
        dict_preprocess={}
        dict_preprocess["Raw Content"]=text
        dict_preprocess["Modified Content"] = response_lemma
        dict_preprocess["Source"] =sr
        dict_preprocess["Spam"] = spam_ham
        return dict_preprocess


data_preprocess = DataPreprocess()
df = pd.read_csv("/home/user/LV/data_scraping/hair care/Upload to S3/aug15-jun16/16_apr_jun.csv",encoding = 'utf-8')
org_li=[]
mod_li=[]
for i in range(10):
    try:
        t=data_preprocess.main(df["CONTENT"][i],df["MEDIA_PROVIDER"][i])
        org_li.append(df["CONTENT"][i])
        mod_li.append(t["Modified Content"])
    except:
        pass
pdf1=pd.DataFrame({"orginal_cont":org_li,"mod_cont":mod_li})
pdf1.to_csv("test.csv")
#df['Spam']=df.iloc[0:10,6].apply(lambda x: data_preprocess.main(text=x))
#print(df.head())
